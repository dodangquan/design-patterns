package DecoratorPattern.example1;

import org.junit.Test;

import DecoratorPattern.example1.FlowerBouquet;
import DecoratorPattern.example1.Glitter;
import DecoratorPattern.example1.OrchidBouquet;
import DecoratorPattern.example1.PaperWrapper;
import DecoratorPattern.example1.RibbonBow;
import DecoratorPattern.example1.RoseBouquet;

public class FlowerBouquetTest {

	@Test
	public void testFlowerBouquet() {
		/* Rose bouquet with no decoration */
		FlowerBouquet roseBouquet = new RoseBouquet();
		System.out.println(roseBouquet.getDescription() + " $ " + roseBouquet.cost());

		/* Rose bouquet with paper wrapper, ribbon bow, and glitter */
		FlowerBouquet decoratedRoseBouquet = new RoseBouquet();
		decoratedRoseBouquet = new PaperWrapper(decoratedRoseBouquet);
		decoratedRoseBouquet = new RibbonBow(decoratedRoseBouquet);
		decoratedRoseBouquet = new Glitter(decoratedRoseBouquet);
		System.out.println(
				decoratedRoseBouquet.getDescription() + " $ " + decoratedRoseBouquet.cost());

		/* Orchid bouquet with double paper wrapper and ribbon bow */
		FlowerBouquet decoratedOrchidBouquet = new OrchidBouquet();
		decoratedOrchidBouquet = new PaperWrapper(decoratedOrchidBouquet);
		decoratedOrchidBouquet = new PaperWrapper(decoratedOrchidBouquet);
		decoratedOrchidBouquet = new RibbonBow(decoratedOrchidBouquet);
		System.out.println(
				decoratedOrchidBouquet.getDescription() + " $ " + decoratedOrchidBouquet.cost());
	}
}
