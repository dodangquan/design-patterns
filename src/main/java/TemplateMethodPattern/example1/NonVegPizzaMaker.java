package TemplateMethodPattern.example1;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public class NonVegPizzaMaker extends PizzaMaker {

	@Override
	public void prepareIngredients() {
		System.out.println("Preparing chicken ham, onion, chicken sausages, and smoked chicken");
	}

	@Override
	public void addToppings() {
		System.out.println(
				"Adding cheese, pepper jelly, and BBQ sauce along with ingredients to crust.");
	}
}
