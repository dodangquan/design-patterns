package FactoryMethodPattern.example1;

// ConcreteCreator
public class XMLLoggerCreator extends AbstractLoggerCreator {

	@Override
	public Logger createLogger() {
		XMLLogger logger = new XMLLogger();
		return logger;
		//throw new IllegalArgumentException("");
	}
}
