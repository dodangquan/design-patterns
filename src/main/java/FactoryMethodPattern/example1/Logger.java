package FactoryMethodPattern.example1;

// interface (Product)
public interface Logger {

	public void log(String message);
}