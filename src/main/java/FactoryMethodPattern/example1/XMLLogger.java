package FactoryMethodPattern.example1;

// concrete implementation of the Logger (Product)
public class XMLLogger implements Logger {

	@Override
	public void log(String message) {

		// log to xml
		System.err.println("logging");
	}
}