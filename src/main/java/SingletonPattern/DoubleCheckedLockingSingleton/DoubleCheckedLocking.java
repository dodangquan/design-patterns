package SingletonPattern.DoubleCheckedLockingSingleton;

/**
 * @author dangquanzt@gmail.com
 */
public class DoubleCheckedLocking {

	private static volatile DoubleCheckedLocking INSTANCE = null;

	private DoubleCheckedLocking() {
		super();
	}

	public static DoubleCheckedLocking getInstance() {
		DoubleCheckedLocking instanceReference = DoubleCheckedLocking.INSTANCE;
		if (instanceReference == null) {
			synchronized (DoubleCheckedLocking.class) {
				instanceReference = DoubleCheckedLocking.INSTANCE;
				if (instanceReference == null) {
					DoubleCheckedLocking.INSTANCE = instanceReference = new DoubleCheckedLocking();
				}
			}
		}
		return INSTANCE;
	}
	
	public void doSomething(){
		// TODO do something
	}
}
