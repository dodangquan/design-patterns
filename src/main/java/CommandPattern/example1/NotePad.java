package CommandPattern.example1;

/**
 * @author Dang Quan Do
 * @author Email: dangquanzt@gmail.com
 * @since Apr 20, 2016
 * @version 1.0
 */
// Client
public class NotePad {

	public static void main(String args[]) {

		Document sampleDoc = new Document();

		// Copy the text using menu item
		MenuItem copy = new MenuItem();
		Command copyCommand = new CopyCommand(sampleDoc);

		copy.setCommand(copyCommand);
		copy.click();

		// Paste the text using menu item
		MenuItem paste = new MenuItem();
		Command pasteCommand = new PasteCommand(sampleDoc);

		paste.setCommand(pasteCommand);
		paste.click();
	}

}
