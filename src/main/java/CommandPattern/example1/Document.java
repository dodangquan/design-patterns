package CommandPattern.example1;

/**
 * @author Dang Quan Do
 * @author Email: dangquanzt@gmail.com
 * @since Apr 20, 2016
 * @version 1.0
 */
// Receiver
public class Document {

	public void copyText() {
		System.out.println("Text copied");
	}

	public void pasteText() {
		System.out.println("Text pasted");
	}
}
