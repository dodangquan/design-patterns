package CommandPattern.example1;

/**
 * @author Dang Quan Do
 * @author Email: dangquanzt@gmail.com
 * @since Apr 20, 2016
 * @version 1.0
 */
// ConcreteCommand2
public class CopyCommand implements Command {

	private Document document;

	public CopyCommand(Document document) {
		this.document = document;
	}

	@Override
	public void execute() {
		document.copyText();
	}
}
