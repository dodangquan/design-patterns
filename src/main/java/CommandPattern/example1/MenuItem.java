package CommandPattern.example1;

/**
 * @author Dang Quan Do
 * @author Email: dangquanzt@gmail.com
 * @since Apr 20, 2016
 * @version 1.0
 */
// Invoker
public class MenuItem {

	private Command command;

	// Set the command to menu item
	public void setCommand(Command command) {
		this.command = command;
	}

	// perform the operation of command by clicking menu item
	public void click() {
		command.execute();
	}
}
