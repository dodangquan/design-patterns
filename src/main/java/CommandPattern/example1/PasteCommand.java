package CommandPattern.example1;

/**
 * @author Dang Quan Do
 * @author Email: dangquanzt@gmail.com
 * @since Apr 20, 2016
 * @version 1.0
 */
// ConcreteCommand1
public class PasteCommand implements Command {

	private Document document;

	PasteCommand(Document document) {
		this.document = document;
	}

	@Override
	public void execute() {
		document.pasteText();
	}

}
