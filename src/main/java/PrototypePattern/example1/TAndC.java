package PrototypePattern.example1;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since May 10, 2016
 */
public class TAndC extends PrototypeCapableDocument {

	@Override
	public PrototypeCapableDocument cloneDocument() {
		/* Clone with shallow copy */
		TAndC tAndC = null;
		try {
			tAndC = (TAndC) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return tAndC;
	}

	@Override
	public String toString() {
		return "[TAndC: Vendor Name - " + getVendorName() + ", Content - " + getContent() + "]";
	}

}
