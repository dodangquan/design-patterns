package PrototypePattern.example2;

import PrototypePattern.example2.PrototypeFactory.ModelType;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since May 10, 2016
 */
public class TestPrototypePattern {

	public static void main(String[] args) {
		try {
			String moviePrototype = PrototypeFactory.getInstance(ModelType.MOVIE).toString();
			System.out.println(moviePrototype);

			String albumPrototype = PrototypeFactory.getInstance(ModelType.ALBUM).toString();
			System.out.println(albumPrototype);

			String showPrototype = PrototypeFactory.getInstance(ModelType.SHOW).toString();
			System.out.println(showPrototype);

		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
	}
}
