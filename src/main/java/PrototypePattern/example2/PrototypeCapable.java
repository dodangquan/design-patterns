package PrototypePattern.example2;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since May 10, 2016
 */
public interface PrototypeCapable extends Cloneable {

	public PrototypeCapable clone() throws CloneNotSupportedException;
}
