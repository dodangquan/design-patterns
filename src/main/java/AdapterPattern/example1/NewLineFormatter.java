package AdapterPattern.example1;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 26, 2016
 * @version 1.0
 */
public class NewLineFormatter implements TextFormattable {

	@Override
	public String formatText(String text) {
		String formattedText = text.replace(".", "\n");
		return formattedText;
	}

}
