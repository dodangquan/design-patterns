package AdapterPattern.example1;


/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 26, 2016
 * @version 1.0
 */
public interface TextFormattable {

	String formatText(String text);

}
