package BridgePattern.example1;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public class TextMessageSender implements MessageSender {

	@Override
	public void sendMessage() {
		System.out.println("TextMessageSender: Sending text message...");
	}
}
