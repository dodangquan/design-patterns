package BridgePattern.example1;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public class EmailMessageSender implements MessageSender {

	@Override
	public void sendMessage() {
		System.out.println("EmailMessageSender: Sending email message...");
	}
}
