package BridgePattern.example1;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public abstract class Message {

	MessageSender messageSender;

	public Message(MessageSender messageSender) {
		this.messageSender = messageSender;
	}

	abstract public void send();
}
