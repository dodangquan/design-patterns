package AbstractFactoryPattern.example2;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 26, 2016
 * @version 1.0
 */
public abstract class Pizza {

	public abstract void addIngredients();

	public void bakePizza() {
		System.out.println("Pizza baked at 400 for 20 minutes.");
	}
}
