package AbstractFactoryPattern.example2;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 26, 2016
 * @version 1.0
 */
public abstract class BaseToppingFactory {

	public abstract Cheese createCheese();

	public abstract Sauce createSauce();
}
