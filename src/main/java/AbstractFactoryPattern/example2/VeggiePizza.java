package AbstractFactoryPattern.example2;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 26, 2016
 * @version 1.0
 */
public class VeggiePizza extends Pizza {

	BaseToppingFactory toppingFactory;

	public VeggiePizza(BaseToppingFactory toppingFactory) {
		this.toppingFactory = toppingFactory;
	}

	@Override
	public void addIngredients() {

		System.out.println("Preparing ingredients for veggie pizza.");
		toppingFactory.createCheese();
		toppingFactory.createSauce();

	}
}
