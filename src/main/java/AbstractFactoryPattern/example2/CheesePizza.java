package AbstractFactoryPattern.example2;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 26, 2016
 * @version 1.0
 */
public class CheesePizza extends Pizza {

	BaseToppingFactory toppingFactory;

	public CheesePizza(BaseToppingFactory toppingFactory) {
		this.toppingFactory = toppingFactory;
	}

	@Override
	public void addIngredients() {
		System.out.println("Preparing ingredients for cheese pizza.");
		toppingFactory.createCheese();
		toppingFactory.createSauce();
	}

}
