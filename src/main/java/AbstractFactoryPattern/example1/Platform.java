package AbstractFactoryPattern.example1;

import java.util.Random;

public class Platform {

	private static final String MACOSX = "MACOSX";
	private static final String MSWINDOWS = "MSWINDOWS";

	public static String currentPlatform() {
		int i = new Random().nextInt(100);
		if (i % 2 == 0) {
			return MACOSX;
		} else {
			return MSWINDOWS;
		}
	}

}
