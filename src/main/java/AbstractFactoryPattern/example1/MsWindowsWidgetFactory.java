package AbstractFactoryPattern.example1;

// ConcreteFactory1
public class MsWindowsWidgetFactory implements AbstractWidgetFactory {

	// Create an MSWindow
	@Override
	public Window createWindow() {
		MSWindow window = new MSWindow();
		return window;
	}
}