package AbstractFactoryPattern.example1;

// ConcreteFactory2
public class MacOSXWidgetFactory implements AbstractWidgetFactory {

	// Create a MacOSXWindow
	@Override
	public Window createWindow() {
		MacOSXWindow window = new MacOSXWindow();
		return window;
	}
}