package AbstractFactoryPattern.example1;

// AbstractFactory
public interface AbstractWidgetFactory {

	public Window createWindow();
}