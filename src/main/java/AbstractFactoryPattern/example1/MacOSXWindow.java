package AbstractFactoryPattern.example1;

// ConcreteProductA2
public class MacOSXWindow implements Window {

	@Override
	public void setTitle(String text) {
		// Mac OSX specific behaviour
		System.out.println(text);
	}

	@Override
	public void rePaint() {
		// Mac OSX specific behaviour
		System.out.println("MacOS");
	}

}
