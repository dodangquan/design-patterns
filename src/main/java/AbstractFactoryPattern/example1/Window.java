package AbstractFactoryPattern.example1;

// Our AbstractProduct
public interface Window {

	public void setTitle(String text);

	public void rePaint();
}
