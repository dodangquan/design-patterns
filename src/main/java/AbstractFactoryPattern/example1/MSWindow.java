package AbstractFactoryPattern.example1;

// ConcreteProductA1
public class MSWindow implements Window {

	@Override
	public void setTitle(String text) {
		// MS Windows specific behavior
		System.out.println(text);
	}

	@Override
	public void rePaint() {
		// MS Windows specific behavior
		System.out.println("MSWindows");
	}
}
