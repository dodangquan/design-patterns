package DecoratorPattern.example1;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public abstract class FlowerBouquetDecorator extends FlowerBouquet {
	
	protected FlowerBouquet flowerBouquet;;

	@Override
	public abstract String getDescription();

}
