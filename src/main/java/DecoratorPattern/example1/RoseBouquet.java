package DecoratorPattern.example1;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public class RoseBouquet extends FlowerBouquet {

	public RoseBouquet() {
		description = "Rose bouquet";
	}

	@Override
	public double cost() {
		return 12.0;
	}
}
