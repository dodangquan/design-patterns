package DecoratorPattern.example1;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public class RibbonBow extends FlowerBouquetDecorator {

	public RibbonBow(FlowerBouquet flowerBouquet) {
		this.flowerBouquet = flowerBouquet;
	}

	@Override
	public String getDescription() {
		return flowerBouquet.getDescription() + ", ribbon bow";
	}

	@Override
	public double cost() {
		return 6.5 + flowerBouquet.cost();
	}
}
