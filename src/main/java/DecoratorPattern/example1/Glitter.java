package DecoratorPattern.example1;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public class Glitter extends FlowerBouquetDecorator {

	public Glitter(FlowerBouquet flowerBouquet) {
		this.flowerBouquet = flowerBouquet;
	}

	@Override
	public String getDescription() {
		return flowerBouquet.getDescription() + ", glitter";
	}

	@Override
	public double cost() {
		return 4 + flowerBouquet.cost();
	}
}
