package DecoratorPattern.example1;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public abstract class FlowerBouquet {

	protected String description;

	public String getDescription() {
		return description;
	}

	public abstract double cost();

	
	public void setDescription(String description) {
		this.description = description;
	}
}
