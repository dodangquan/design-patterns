package DecoratorPattern.example1;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public class PaperWrapper extends FlowerBouquetDecorator {

	public PaperWrapper(FlowerBouquet flowerBouquet) {
		this.flowerBouquet = flowerBouquet;
	}

	@Override
	public String getDescription() {
		return flowerBouquet.getDescription() + ", paper wrap";
	}

	@Override
	public double cost() {
		return 3 + flowerBouquet.cost();
	}
}
