package DecoratorPattern.example1;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public class OrchidBouquet extends FlowerBouquet {

	public OrchidBouquet() {
		description = "Orchid bouquet";
	}

	@Override
	public double cost() {
		return 29.0;
	}
}
