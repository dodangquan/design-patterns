package StatePattern.example1;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
// ConreteStateB
public class DvdStateNameStars implements DvdStateName {

	private int starCount;

	public DvdStateNameStars() {
		starCount = 0;
	}

	@Override
	public void showName(DvdStateContext dvdStateContext, String nameIn) {
		System.out.println(nameIn.replace(' ', '*'));
		// show stars twice, switch to exclamation point
		if (++starCount > 1) {
			dvdStateContext.setDvdStateName(new DvdStateNameExclaim());
		}
	}

	public int getStarCount() {
		return starCount;
	}

	public void setStarCount(int starCount) {
		this.starCount = starCount;
	}
}
