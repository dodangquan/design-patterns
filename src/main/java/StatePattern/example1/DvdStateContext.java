package StatePattern.example1;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
// Context
public class DvdStateContext {

	private DvdStateName dvdStateName;

	// request()
	public DvdStateContext() {
		setDvdStateName(new DvdStateNameStars());
		// start with stars
	}

	public void setDvdStateName(DvdStateName dvdStateNameIn) {
		this.dvdStateName = dvdStateNameIn;
	}

	public void showName(String nameIn) {
		this.dvdStateName.showName(this, nameIn);
	}
}
