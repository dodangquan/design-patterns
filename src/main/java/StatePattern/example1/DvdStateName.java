package StatePattern.example1;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public interface DvdStateName {

	void showName(DvdStateContext dvdStateContext, String nameIn);
}
