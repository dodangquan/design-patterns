package StatePattern.example1;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
// ConreteStateA
public class DvdStateNameExclaim implements DvdStateName {

	public DvdStateNameExclaim() {
	}

	@Override
	public void showName(DvdStateContext dvdStateContext, String nameIn) {
		System.out.println(nameIn.replace(' ', '!'));
		// show exclaim only once, switch back to stars
		dvdStateContext.setDvdStateName(new DvdStateNameStars());
	}
}
