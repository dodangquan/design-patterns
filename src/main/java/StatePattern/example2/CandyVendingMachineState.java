package StatePattern.example2;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public interface CandyVendingMachineState {

	void insertCoin();

	void pressButton();

	void dispense();
}
