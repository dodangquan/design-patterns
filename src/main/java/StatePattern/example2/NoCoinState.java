package StatePattern.example2;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public class NoCoinState implements CandyVendingMachineState {

	CandyVendingMachine machine;

	public NoCoinState(CandyVendingMachine machine) {
		this.machine = machine;
	}

	@Override
	public void insertCoin() {
		machine.setState(machine.getContainsCoinState());
	}

	@Override
	public void pressButton() {
		System.out.println("No coin inserted");
	}

	@Override
	public void dispense() {
		System.out.println("No coin inserted");
	}

	@Override
	public String toString() {
		return "NoCoinState";
	}
}
