package StatePattern.example2;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public class ContainsCoinState implements CandyVendingMachineState {

	CandyVendingMachine machine;

	public ContainsCoinState(CandyVendingMachine machine) {
		this.machine = machine;
	}

	@Override
	public void insertCoin() {
		System.out.println("Coin already inserted");
	}

	@Override
	public void pressButton() {
		machine.setState(machine.getDispensedState());
	}

	@Override
	public void dispense() {
		System.out.println("Press button to dispense");
	}

	@Override
	public String toString() {
		return "ContainsCoinState";
	}
}
