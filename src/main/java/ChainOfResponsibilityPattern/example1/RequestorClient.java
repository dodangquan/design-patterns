package ChainOfResponsibilityPattern.example1;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 26, 2016
 * @version 1.0
 */
public class RequestorClient {

	public static AbstractSupportHandler getHandlerChain() {

		AbstractSupportHandler technicalSupportHandler = new TechnicalSupportHandler(
				AbstractSupportHandler.TECHNICAL);
		AbstractSupportHandler billingSupportHandler = new BillingSupportHandler(
				AbstractSupportHandler.BILLING);
		AbstractSupportHandler generalSupportHandler = new GeneralSupportHandler(
				AbstractSupportHandler.GENERAL);

		technicalSupportHandler.setNextHandler(billingSupportHandler);
		billingSupportHandler.setNextHandler(generalSupportHandler);

		return technicalSupportHandler;
	}

}