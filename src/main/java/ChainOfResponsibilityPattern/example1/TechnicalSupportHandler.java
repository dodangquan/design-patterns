package ChainOfResponsibilityPattern.example1;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 26, 2016
 * @version 1.0
 */
public class TechnicalSupportHandler extends AbstractSupportHandler {

	public TechnicalSupportHandler(int level) {
		this.level = level;
	}

	@Override
	protected void handleRequest(String message) {
		System.out.println("TechnicalSupportHandler: Processing request " + message);

	}
}