package ChainOfResponsibilityPattern.example1;


/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 26, 2016
 * @version 1.0
 */
public class BillingSupportHandler extends AbstractSupportHandler {
	 
    public BillingSupportHandler(int level){
        this.level = level;
    }
 
        @Override
        protected void handleRequest (String message){
        System.out.println("BillingSupportHandler: Processing request. " + message);
    }
 
}
