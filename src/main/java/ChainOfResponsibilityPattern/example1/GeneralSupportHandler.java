package ChainOfResponsibilityPattern.example1;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 26, 2016
 * @version 1.0
 */
public class GeneralSupportHandler extends AbstractSupportHandler {

	public GeneralSupportHandler(int level) {
		this.level = level;
	}

	@Override
	protected void handleRequest(String message) {
		System.out.println("GeneralSupportHandler: Processing request. " + message);

	}
}
