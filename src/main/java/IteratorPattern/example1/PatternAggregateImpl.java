package IteratorPattern.example1;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public class PatternAggregateImpl implements PatternAggregate {

	private List<DesignPattern> patternList;

	public PatternAggregateImpl() {
		patternList = new ArrayList<DesignPattern>();
	}

	@Override
	public void addPattern(DesignPattern designPattern) {
		patternList.add(designPattern);
	}

	@Override
	public void removePattern(DesignPattern designPattern) {
		patternList.remove(designPattern);
	}

	@Override
	public PatternIterator getPatternIterator() {
		return new PatternIteratorImpl(patternList);
	}
}
