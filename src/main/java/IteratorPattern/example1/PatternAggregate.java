package IteratorPattern.example1;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public interface PatternAggregate {

	void addPattern(DesignPattern designPattern);

	void removePattern(DesignPattern designPattern);

	PatternIterator getPatternIterator();
}
