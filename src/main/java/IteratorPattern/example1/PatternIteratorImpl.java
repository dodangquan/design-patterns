package IteratorPattern.example1;

import java.util.List;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public class PatternIteratorImpl implements PatternIterator {

	private List<DesignPattern> patternList;
	private int position;
	private DesignPattern designPattern;

	public PatternIteratorImpl(List<DesignPattern> patternList) {
		this.patternList = patternList;
	}

	@Override
	public DesignPattern nextPattern() {
		System.out.println("Returning pattern at Position: " + position);
		designPattern = (DesignPattern) patternList.get(position);
		position++;
		return designPattern;
	}

	@Override
	public boolean isLastPattern() {
		if (position < patternList.size()) {
			return false;
		}
		return true;
	}

}
