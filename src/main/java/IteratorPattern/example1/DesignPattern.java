package IteratorPattern.example1;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public class DesignPattern {

	private String patternType;
	private String patternName;

	public DesignPattern(String patternType, String patternName) {
		this.patternType = patternType;
		this.patternName = patternName;
	}

	public String getPatternType() {

		return patternType;
	}

	public String getPatternName() {
		return patternName;
	}
}
