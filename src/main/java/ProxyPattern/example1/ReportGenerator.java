package ProxyPattern.example1;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public interface ReportGenerator {

	void displayReportTemplate(String reportFormat, int reportEntries);

	void generateComplexReport(String reportFormat, int reportEntries);

	void generateSensitiveReport();
}
