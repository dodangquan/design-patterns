package FlyweightPattern.example1;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
// Flyweight
public abstract class RaceCar {

	/* Intrinsic state stored and shared in the Flyweight object */
	String name;
	int speed;
	int horsePower;

	/*
	 * Extrinsic state is stored or computed by client objects, and passed to
	 * the Flyweight.
	 */
	abstract void moveCar(int currentX, int currentY, int newX, int newY);
}
