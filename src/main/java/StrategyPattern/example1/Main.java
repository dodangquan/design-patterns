package StrategyPattern.example1;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public class Main {

	public static void main(String[] args) {
		EncryptionStrategy aesStrategy = new AesEncryptionStrategy();
		Encryptor aesEncryptor = new Encryptor(aesStrategy);
		aesEncryptor.setPlainText("This is plain text");
		aesEncryptor.encrypt();

		EncryptionStrategy blowfishStrategy = new BlowfishEncryptionStrategy();
		Encryptor blowfishEncryptor = new Encryptor(blowfishStrategy);
		blowfishEncryptor.setPlainText("This is plain text");
		blowfishEncryptor.encrypt();
	}
}
