package StrategyPattern.example1;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
// Context
public class Encryptor {

	private EncryptionStrategy strategy;
	private String plainText;

	public Encryptor(EncryptionStrategy strategy) {
		this.strategy = strategy;
	}

	public void encrypt() {
		strategy.encryptData(plainText);
	}

	public String getPlainText() {
		return plainText;
	}

	public void setPlainText(String plainText) {
		this.plainText = plainText;
	}

}