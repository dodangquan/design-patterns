package StrategyPattern.example2;

import java.io.File;
import java.util.ArrayList;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public class CompressionContext {

	private CompressionStrategy strategy;

	// this can be set at runtime by the application preferences
	public void setCompressionStrategy(CompressionStrategy strategy) {
		this.strategy = strategy;
	}

	// use the strategy
	public void createArchive(ArrayList<File> files) {
		strategy.compressFiles(files);
	}

}
