package StrategyPattern.example2;

import java.io.File;
import java.util.ArrayList;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public class Client {

	public static void main(String[] args) {
		CompressionContext ctx = new CompressionContext();

		// we could assume context is already set by preferences
		ctx.setCompressionStrategy(new ZipCompressionStrategy());

		ArrayList<File> fileList = new ArrayList<>();

		// get a list of files...
		ctx.createArchive(fileList);
	}
}
