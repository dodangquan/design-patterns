package StrategyPattern.example2;

import java.io.File;
import java.util.ArrayList;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public interface CompressionStrategy {

	void compressFiles(ArrayList<File> files);
}
