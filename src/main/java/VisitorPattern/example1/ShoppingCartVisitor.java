package VisitorPattern.example1;

public interface ShoppingCartVisitor {

	int visit(Book book);

	int visit(Fruit fruit);
}
