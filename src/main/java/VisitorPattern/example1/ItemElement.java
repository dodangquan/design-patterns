package VisitorPattern.example1;

public interface ItemElement {

	public int accept(ShoppingCartVisitor visitor);
}
