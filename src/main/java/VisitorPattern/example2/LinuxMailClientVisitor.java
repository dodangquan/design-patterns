package VisitorPattern.example2;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 26, 2016
 * @version 1.0
 */
public class LinuxMailClientVisitor implements MailClientVisitor {

	@Override
	public void visit(OperaMailClient mailClient) {
		System.out.println("Configuration of Opera mail client for Linux complete");
	}

	@Override
	public void visit(SquirrelMailClient mailClient) {
		System.out.println("Configuration of Squirrel mail client for Linux complete");

	}

	@Override
	public void visit(ZimbraMailClient mailClient) {
		System.out.println("Configuration of Zimbra mail client for Linux complete");

	}
}
