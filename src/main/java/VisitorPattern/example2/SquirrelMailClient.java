package VisitorPattern.example2;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 26, 2016
 * @version 1.0
 */
public class SquirrelMailClient implements MailClient {

	@Override
	public void sendMail(String[] mailInfo) {
		System.out.println(" SquirrelMailClient: Sending mail");
	}

	@Override
	public void receiveMail(String[] mailInfo) {
		System.out.println(" SquirrelMailClient: Receiving mail");
	}

	@Override
	public boolean accept(MailClientVisitor visitor) {
		visitor.visit(this);
		return true;
	}
}
