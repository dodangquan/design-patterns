package VisitorPattern.example2;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 26, 2016
 * @version 1.0
 */
public interface MailClient {

	void sendMail(String[] mailInfo);

	void receiveMail(String[] mailInfo);

	boolean accept(MailClientVisitor visitor);
}
