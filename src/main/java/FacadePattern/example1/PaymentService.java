package FacadePattern.example1;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public class PaymentService {

	public static boolean makePayment() {
		/* Connect with payment gateway for payment */
		return true;
	}
}
