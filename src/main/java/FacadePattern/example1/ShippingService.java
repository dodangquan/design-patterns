package FacadePattern.example1;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public class ShippingService {

	public static void shipProduct(Product product) {
		/* Connect with external shipment service to ship product */
	}
}
