package FacadePattern.example1;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public class OrderServiceFacadeImpl implements OrderServiceFacade {

	@Override
	public boolean placeOrder(int pId) {
		boolean orderFulfilled = false;
		Product product = new Product();
		product.setProductId(pId);
		if (InventoryService.isAvailable(product)) {
			System.out.println("Product with ID: " + product.getProductId() + " is available.");
			boolean paymentConfirmed = PaymentService.makePayment();
			if (paymentConfirmed) {
				System.out.println("Payment confirmed...");
				ShippingService.shipProduct(product);
				System.out.println("Product shipped...");
				orderFulfilled = true;
			}
		}
		return orderFulfilled;
	}
}
