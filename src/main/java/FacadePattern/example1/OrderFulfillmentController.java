package FacadePattern.example1;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public class OrderFulfillmentController {

	OrderServiceFacade facade;
	boolean orderFulfilled = false;

	public void orderProduct(int productId) {
		orderFulfilled = facade.placeOrder(productId);
		System.out.println("OrderFulfillmentController: Order fulfillment completed. ");
	}
}
