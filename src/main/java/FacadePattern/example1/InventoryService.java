package FacadePattern.example1;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public class InventoryService {

	public static boolean isAvailable(Product product) {
		/* Check Warehouse database for product availability */
		return true;
	}
}
