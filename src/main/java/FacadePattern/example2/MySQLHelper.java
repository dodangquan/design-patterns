package FacadePattern.example2;

import java.sql.Connection;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public class MySQLHelper {

	public static Connection getMySQLDBConnection() {
		System.out.println("Connect MySQL Database");
		return null;
	}

	public void generateMySQLPDFReport(String tableName, Connection con) {
		// get data from table and generate pdf report
		System.out.println("get data from table and generate pdf report (MySQL)");
	}

	public void generateMySQLHTMLReport(String tableName, Connection con) {
		// get data from table and generate pdf report
		System.out.println("get data from table and generate pdf report (MySQL)");
	}
}
