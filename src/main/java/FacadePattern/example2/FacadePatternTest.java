package FacadePattern.example2;

import java.sql.Connection;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public class FacadePatternTest {

	public static void main(String[] args) {
		String tableName = "Employee";

		// generating MySql HTML report and Oracle PDF report without using
		// Facade
		System.out.println("generating MySql HTML report and Oracle PDF report without using Facade");
		
		Connection con = MySQLHelper.getMySQLDBConnection();
		MySQLHelper mySqlHelper = new MySQLHelper();
		mySqlHelper.generateMySQLHTMLReport(tableName, con);

		Connection con1 = OracleHelper.getOracleDBConnection();
		OracleHelper oracleHelper = new OracleHelper();
		oracleHelper.generateOraclePDFReport(tableName, con1);

		System.out.println();
		
		// generating MySql HTML report and Oracle PDF report using Facade
		System.out.println("generating MySql HTML report and Oracle PDF report using Facade");
		
		HelperFacade.generateReport(DBTypes.MYSQL, ReportTypes.HTML, tableName);
		HelperFacade.generateReport(DBTypes.ORACLE, ReportTypes.PDF, tableName);
	}

}
