package FacadePattern.example2;


/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public enum DBTypes {
	MYSQL, ORACLE;
}
