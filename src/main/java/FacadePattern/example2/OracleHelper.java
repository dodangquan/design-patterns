package FacadePattern.example2;

import java.sql.Connection;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public class OracleHelper {

	public static Connection getOracleDBConnection() {
		// get Oracle DB connection using connection parameters
		System.out.println("Connect Oracle Database");
		return null;
	}

	public void generateOraclePDFReport(String tableName, Connection con) {
		// get data from table and generate pdf report
		System.out.println("get data from table and generate pdf report (Oracle)");
	}

	public void generateOracleHTMLReport(String tableName, Connection con) {
		// get data from table and generate pdf report
		System.out.println("get data from table and generate pdf report (Oracle)");
	}
}
