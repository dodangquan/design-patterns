package FacadePattern.example2;

import java.sql.Connection;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */
public class HelperFacade {

	public static void generateReport(DBTypes dbType, ReportTypes reportType, String tableName) {
		Connection con = null;
		switch (dbType) {
		case MYSQL:
			con = MySQLHelper.getMySQLDBConnection();
			MySQLHelper mySqlHelper = new MySQLHelper();
			switch (reportType) {
			case HTML:
				mySqlHelper.generateMySQLHTMLReport(tableName, con);
				break;
			case PDF:
				mySqlHelper.generateMySQLPDFReport(tableName, con);
				break;
			}
			break;
		case ORACLE:
			con = OracleHelper.getOracleDBConnection();
			OracleHelper oracleHelper = new OracleHelper();
			switch (reportType) {
			case HTML:
				oracleHelper.generateOracleHTMLReport(tableName, con);
				break;
			case PDF:
				oracleHelper.generateOraclePDFReport(tableName, con);
				break;
			}
			break;
		}

	}
}
