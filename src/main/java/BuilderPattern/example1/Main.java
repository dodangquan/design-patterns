package BuilderPattern.example1;

import java.util.Random;

/**
 * @author Dang Quan Do
 * @author Email: dangquanzt@gmail.com
 * @since Apr 15, 2016
 * @version 1.0
 */
// Integration with overall application
public class Main {

	public static void main(String[] args) {
		MealBuilder mealBuilder = null;
		boolean isKid;
		Random rd = new Random();
		int i = rd.nextInt(10) + 1;
		if (i < 6) {
			isKid = true;
		} else {
			isKid = false;
		}
		if (isKid) {
			mealBuilder = new KidsMealBuilder();
		} else {
			mealBuilder = new AdultMealBuilder();
			mealBuilder.buildDessert();
			mealBuilder.buildDrink();
			mealBuilder.buildDessert();
		}
		MealDirector director = new MealDirector(mealBuilder);
		Meal meal = director.createMeal();
		System.out.println(meal.getMain());
	}
}
