package BuilderPattern.example1;

/**
 * @author Dang Quan Do
 * @author Email: dangquanzt@gmail.com
 * @since Apr 15, 2016
 * @version 1.0
 */
// Director
public class MealDirector {

	private MealBuilder builder;

	public MealDirector(MealBuilder builder) {
		this.builder = builder;
	}

	public MealDirector() {
		super();
	}

	public Meal createMeal() {
		builder.buildDrink();
		builder.buildMain();
		builder.buildDessert();
		return builder.getMeal();
	}
}