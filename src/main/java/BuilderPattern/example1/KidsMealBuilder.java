package BuilderPattern.example1;

/**
 * @author Dang Quan Do
 * @author Email: dangquanzt@gmail.com
 * @since Apr 15, 2016
 * @version 1.0
 */
// ConcreteBuilder B
public class KidsMealBuilder extends MealBuilder {

	@Override
	public void buildDrink() {
		// add drinks to the meal
		meal.setDrink("Kid Drink");
	}

	@Override
	public void buildMain() {
		// add main part of the meal
		meal.setMain("Kid Main");
	}

	@Override
	public void buildDessert() {
		// add dessert part to the meal
		meal.setDessert("Kid Dessert");
	}

	@Override
	public Meal getMeal() {
		return meal;
	}

}
