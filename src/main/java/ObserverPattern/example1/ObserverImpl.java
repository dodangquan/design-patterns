package ObserverPattern.example1;

/**
 * @author Dang Quan Do
 * @author Email: dangquanzt@gmail.com
 * @since Apr 20, 2016
 * @version 1.0
 */
// Concrete Observer
public class ObserverImpl implements Observer {

	private Subject subject;
	private String name;

	public ObserverImpl(String name, Subject subject) {
		this.subject = subject;
		this.name = name;
		subject.registerObserver(this);
	}

	@Override
	public void update(String message) {
		System.out.println(name + " received newspaper of " + message);
	}

	public void unSubscribe(Subject subject) {
		subject.removeObserver(this);
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
