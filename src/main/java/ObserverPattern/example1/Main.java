package ObserverPattern.example1;

/**
 * @author Dang Quan Do
 * @author Email: dangquanzt@gmail.com
 * @since Apr 20, 2016
 * @version 1.0
 */
public class Main {

	public static void main(String[] args) {
		SubjectImpl newsPaper = new SubjectImpl();
		ObserverImpl Jack = new ObserverImpl("Jack", newsPaper);
		ObserverImpl Sparrow = new ObserverImpl("Sparrow", newsPaper);

		newsPaper.publish("14 September");
		newsPaper.publish("15 September");
		newsPaper.publish("16 September");

		// Jack doesn't want newspaper anymore
		Jack.unSubscribe(newsPaper);

		// Only Sparrow will receive newspaper henceforth
		newsPaper.publish("17 September");

		// Sparrow doesn't want newspaper anymore
		Sparrow.unSubscribe(newsPaper);

		newsPaper.publish("18 September");
	}
}
