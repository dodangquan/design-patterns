package ObserverPattern.example1;

/**
 * @author Dang Quan Do
 * @author Email: dangquanzt@gmail.com
 * @since Apr 20, 2016
 * @version 1.0
 */
// Subject
public interface Subject {

	void registerObserver(Observer observer);

	void removeObserver(Observer observer);

	void notifyObersevers();
}
