package ObserverPattern.example1;

import java.util.ArrayList;

/**
 * @author Dang Quan Do
 * @author Email: dangquanzt@gmail.com
 * @since Apr 20, 2016
 * @version 1.0
 */
// ConcreteSubject
public class SubjectImpl implements Subject {

	private ArrayList<Observer> listOfObservers;
	private String message;

	public SubjectImpl() {
		listOfObservers = new ArrayList<Observer>();
	}

	@Override
	public void registerObserver(Observer observer) {
		listOfObservers.add(observer);
	}

	@Override
	public void removeObserver(Observer observer) {
		listOfObservers.remove(observer);
	}

	@Override
	public void notifyObersevers() {
		int size = listOfObservers.size();

		if (size == 0) {
			System.out.println("No followers!");
			return;
		}

		for (int i = 0; i < size; i++) {
			Observer observer = listOfObservers.get(i);
			observer.update(message);
		}
		System.out.println();
	}

	public void publish(String message) {
		this.message = message;
		notifyObersevers();
	}

}
