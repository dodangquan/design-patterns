package MementoPattern.example2;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * @author Dang Quan Do
 * @author dangquanzt@gmail.com
 * @since Apr 23, 2016
 * @version 1.0
 */

public class EmpCaretaker {

	final Deque<EmpMemento> mementos = new ArrayDeque<>();

	public EmpMemento getMemento() {

		EmpMemento empMemento = mementos.pop();
		return empMemento;
	}

	public void addMemento(EmpMemento memento) {
		mementos.push(memento);

	}
}
