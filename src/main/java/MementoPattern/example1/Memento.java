package MementoPattern.example1;

/**
 * @author Dang Quan Do
 * @author Email: dangquanzt@gmail.com
 * @since Apr 18, 2016
 * @version 1.0
 */
public class Memento {

	private String state;

	public Memento(String stateToSave) {
		this.state = stateToSave;
	}

	public String getSavedState() {
		return state;
	}
}
