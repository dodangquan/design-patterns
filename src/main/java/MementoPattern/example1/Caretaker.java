package MementoPattern.example1;

import java.util.ArrayList;

/**
 * @author Dang Quan Do
 * @author Email: dangquanzt@gmail.com
 * @since Apr 18, 2016
 * @version 1.0
 */
public class Caretaker {

	private ArrayList<Memento> savedStateds = new ArrayList<>();

	public void addMemento(Memento m) {
		savedStateds.add(m);
	}

	public Memento getMemento(int index) {
		return savedStateds.get(index);
	}
}
