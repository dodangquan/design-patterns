package MementoPattern.example1;

/**
 * @author Dang Quan Do
 * @author Email: dangquanzt@gmail.com
 * @since Apr 18, 2016
 * @version 1.0
 */
public class MementoExample {

	public static void main(String[] args) {
		Caretaker caretaker = new Caretaker();

		Originator originator = new Originator();
		originator.set("Sate 1");
		originator.set("State 2");
		caretaker.addMemento(originator.saveToMemento());

		originator.set("State 3");
		caretaker.addMemento(originator.saveToMemento());

		originator.set("State 4");

		originator.restoreFromMemento(caretaker.getMemento(1));
	}
}
